# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/hyprland/issues
# Bug-Submit: https://github.com/<user>/hyprland/issues/new
# Changelog: https://github.com/<user>/hyprland/blob/master/CHANGES
# Documentation: https://github.com/<user>/hyprland/wiki
# Repository-Browse: https://github.com/<user>/hyprland
# Repository: https://github.com/<user>/hyprland.git
